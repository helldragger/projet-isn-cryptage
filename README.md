# CRYPTOTRON #

Ceci est le projet d'ISN de Pagnoux Guillaume et Jacquiot Christopher, logiciel de dé/cryptage et de rétrospective sur les différentes méthodes de cryptages a travers les âges jusqu'à aujourd'hui.

### A quoi sert ce repertoire?? ###

* Sous l'onglet sources vous pourrez acceder au contenu des fichiers sources de notre projet afinde les consulter en ligne
* Sous l'onglet téléchargement vous pourrez telecharger la derniere version des sources afin de pouvoir tester le logiciel sur votre pc.
* Version finale

### Comment installer le cryptotron? ###

* Telechargez les sources via l'onglet téléchargement
* Lancez l'interface de votre choix (start.py ou startbychris.py)

### De quoi a t on besoin pour le faire fonctionner? ###

* il est IMPERATIF de posseder la version 3.4 MINIMUM de python, installé sur son ordinateur afin de pouvoir faire fonctionner le programme

### Des question? Qui contacter? ###

* Pour toute question au sujet de ce projet, adressez vous à l'administrateur du répertoire (Helldragger)

Ce projet est une logiciel libre sous license GNU GPL v3