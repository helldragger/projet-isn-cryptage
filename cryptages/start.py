from tkinter import *
import Cryptages

window = Tk()
window.title("Cryptotron")


class Interface(Frame):

#Phase d'initialisation des variables
    currentKey = StringVar() #Variable contenant la cle que l'utilisateur est en train de taper
    currentKey.set('')
    currentKeyNumber = IntVar() #Variable contenant le rang de la cle que l'utilisateur est en tran de taper
    keysNumber = IntVar() #Variable contenant le nombre total de cles necessaire pour le cryptage selectionne
    keyMessageText = StringVar() #Variable contenant le texte du message indiquant combien de cles sont necessaires
    keyMessageText.set('Veuillez sélectionner un cryptage')
    keyInfo = StringVar() #Variable contenant les informations sur la cle actuelle
    keysInfos = [] #Liste contenant les informations sur toutes les cles du cryptage
    transformChoice = IntVar() #Variable contenant le choix de l'utilisateur sur l'action a effectuer (crypter ou decrypter)
    choice = StringVar() #Variable contenant le nom du cryptage choisi
    upButtonText = StringVar() #Variable contenant le texte affiche par le bouton superieur
    upButtonText.set('                 ')
    downButtonText = StringVar() #Variable contenant le texte affiche par le bouton inferieur
    downButtonText.set('                 ')
    result = StringVar() #Variable contenant le resultat de l'algorithme de cryptage/decryptage choisi
    result.set('')

    keys = [] #Initialisation de la liste contenant les differentes cles entrees par l'utilisateur

    def __init__(self, window):

        #Definition des proprietes de la fenetre
        window.minsize(width = 600, height = 100)
        Frame.__init__(self, window, width=600, height=100)
        self.grid()
        #Creation des frames
        # Frame contenant la zone d'entree des cles ainsi que la Frame contenant les boutons
        keyFrame = Frame(self, relief = 'groove', borderwidth = 2, width = 1000)
        keyFrame.grid_columnconfigure(2, minsize = 200)
        keyFrame.grid(column = 0, columnspan = 2, row = 2, sticky = 'W', padx = 5)

        # Frame contenant les boutons
        buttonFrame = Frame(keyFrame, relief = 'groove', borderwidth = 2)
        buttonFrame.grid(column = 2, row = 1, padx = 3, pady = 3)

        # Frame contenant le resultat du cryptage/decryptage
        bottomFrame = Frame(window, relief = 'groove', borderwidth = 2, width = 1000)
        bottomFrame.grid(column = 0, row = 3, padx = 5, pady = 2)


        #Creation de la barre de menu
        menubar = Menu(self) #Initialisation de l'objet menu
        menubar.add_command(label="Informations sur le cryptage", command = self.infobutton) #Creation du bouton d'informations sur les cryptages
        menubar.add_command(label="Quitter", command = self.quit) #Creation du bouton quitter

        window.config(menu = menubar) #Affichage de la barre de menu

        #Creation des widgets
        listMessage = Message(self, width = 180, text="Liste des cryptages")
        listMessage.grid(column = 0, row = 0)

        # Creation de la liste de cryptages
        cryptoList = Listbox(self, selectmode = SINGLE, width = 30) #Definition de la liste. Un seul cryptage est selectionnable
        cryptoList.bind('<<ListboxSelect>>', self.onselect) # Execute la fonction 'onselect' quand un cryptage est selectionne
        for item in Cryptages.Cryptage: # Insertion des differents cryptages dans la liste
            cryptoList.insert(END, item.getcryptagename)
        cryptoList.grid(column = 0, row = 1, pady = 5, padx = 3) # Affichage de la liste

        cypherMessage = Message(self, width = 250, text="Entrez ici le texte à crypter/décrypter")
        cypherMessage.grid(column = 1, row = 0)
        # Creation de la zone d'entree contenant le texte a crypter
        self.cypher = Text(self, wrap = WORD, width=40, height=10)
        self.cypher.grid(column = 1, row = 1, pady = 5)

        # Creation du bouton superieur
        buttonUp = Button(buttonFrame, textvariable = self.upButtonText, command = self.upbuttonactions) # Appel de la fonction 'upbuttonactions' quand l'utilisateur clique sur le bouton
        buttonUp.grid()

        # Creation du bouton inferieur
        buttonDown = Button(buttonFrame, textvariable = self.downButtonText, command = self.downbuttonactions) # Appel de la fonction 'downbuttonactions' quand l'utilisateur clique sur le bouton
        buttonDown.grid()

        #Creation des boutons radio
        radioCr = Radiobutton(keyFrame, text = 'Crypter', variable = self.transformChoice, value = 0, command = self.buttonSet) # Le bouton affecte la valeur 0 a la variable transformChoice et appelle la fonction 'buttonSet'
        radioDe = Radiobutton(keyFrame, text = 'Décrypter', variable = self.transformChoice, value = 1, command = self.buttonSet) # Le bouton affecte la valeur 1 a la variable transformChoice et appelle la fonction 'buttonSet'
        radioCr.grid(column = 0, row = 0)
        radioDe.grid(column = 1, row = 0)

        keysMessage = Message(keyFrame, textvariable = self.keyMessageText, width = 150) # Message d'informations sur les cles
        keysMessage.grid(column = 2, row = 0)

        keysEntry = Entry(keyFrame, width = 50, bg = 'white', textvariable = self.currentKey) # Zone d'entree des clees de cryptage
        keysEntry.grid(column = 0, row = 1, padx = 5, pady = 5)

        keyInfoMessage = Message(keyFrame, textvariable = self.keyInfo, width = 200) # Message d'information sur la cle actuelle
        keyInfoMessage.grid(column = 0, row = 2, padx = 5)

        resultMessage = Message(bottomFrame, width = 200, text = "Resultat :") # Message affichant 'Resultat :' au dessus de la zone d'affichage du resultat
        resultMessage.grid(column = 0, row = 0)

        resultLabel = Label(bottomFrame, wrap = 500, textvariable = self.result, width = 88, height = 5) # Zone d'affichage du resultat
        resultLabel.grid(column = 0, row = 1)

    def onselect(self, evt): # Fonction exectutee lors de la selection d'un element de la liste de cryptage
        widget = evt.widget #On recupere le widget concerne par l'evenement
        index = int(widget.curselection()[0]) # On recupere l'index de l'item selectionne par l'utilisateur
        self.choice.set(widget.get(index)) #On recupere le nom du cryptage selectionne
        if Cryptages.getcryptagebyname(self.choice.get()).iskeyneeded: # On regarde si une cle est necessaire
            self.currentKeyNumber.set(0) # On initialise l'index de la cle actuelle a 0
            self.keysNumber.set(Cryptages.getcryptagebyname(self.choice.get()).getkeycount) # On recupere le nombre total de cles necessaires pour le cryptage selectionne
            self.keyMessageText.set('Clé ' + str(self.currentKeyNumber.get() + 1) + ' / ' + str(self.keysNumber.get())) # On met a jour le message d'information sur les cles
            self.keys = ['' for x in range(self.keysNumber.get())] # On fait en sorte que la liste des cles rentrees par l'utilisateur soit vide et suffisament grande pour toutes les contenir
            self.keysInfos = Cryptages.getcryptagebyname(self.choice.get()).getkeys # On recupere les informations pour toutes les cles du cryptage

            # Si une seule cle est necessaire alors on fait en sorte que les boutons affichent directement les actions Crypter ou Decrypter
            if self.keysNumber.get() == 1:
                self.keyInfo.set(self.keysInfos[0].name) # On affiche les informations sur la cle
                if self.transformChoice.get() == 0:
                    self.upButtonText.set('  Crypter  ')
                    self.downButtonText.set('                 ')
                else:
                    self.upButtonText.set(' Décrypter ')
                    self.downButtonText.set('                 ')
            # Sinon, le bouton superieur affiche suivant
            else:
                self.keyInfo.set(self.keysInfos[0].name) # On affiche les informations sur la cle
                self.upButtonText.set('  Suivant  ')
                self.downButtonText.set('                 ')
        else: #Si une cle n'est pas necessaire
            self.keyInfo.set('') # On affiche aucune informations sur la cle puisqu'il n'y en a pas
            self.keysNumber.set(0) # On met le nombre de cles necessaires a 0
            self.keys = ['' for x in range(self.keysNumber.get())] # On reinitialise la liste de cles
            self.keyMessageText.set('Aucune clé nécessaire') # On informe l'utilsateur qu'aucune cle n'est necessaire
            # En fonction du bouton radial selectionne, on fixe le texte du bouton superieur
            if self.transformChoice.get() == 0:
                self.upButtonText.set('  Crypter  ')
                self.downButtonText.set('                 ')
            else:
                self.upButtonText.set(' Décrypter ')
                self.downButtonText.set('                 ')

    def upbuttonactions(self): # Fonction executee lors d'un clic sur le bouton superieur
        if self.choice.get() != '': # On verifie tout d'abord que l'utilisateur ai bien selectionne un choix
            if Cryptages.getcryptagebyname(self.choice.get()).iskeyneeded: # On regarde si le cryptage a besoin de cles
                if (self.currentKeyNumber.get() + 1) != self.keysNumber.get(): # Si la cle actuelle n'est pas la derniere cle demandee
                    self.keys[self.currentKeyNumber.get()] = self.currentKey.get() # On ajoute la cle entree a la liste contenant les cles entrees par l'utilisateur
                    if self.currentKeyNumber.get() == 0: # Si la cle rentree etait la premiere alors on affiche le bouton precedent pour pouvoir y retourner
                        self.downButtonText.set('Precedent')
                    self.currentKeyNumber.set(self.currentKeyNumber.get() + 1) # On passe a la cle suivante
                    self.keyMessageText.set('Cle ' + str(self.currentKeyNumber.get() + 1) + ' / ' + str(self.keysNumber.get())) # On met a jour l'affichage pour que le rang de la cle soit mis a jour
                    self.keyInfo.set(self.keysInfos[self.currentKeyNumber.get()].name) # On affiche les informations de la nouvelle cle

                    if (self.currentKeyNumber.get() + 1) == self.keysNumber.get(): # Si la cle suivante est la derniere, on affiche le bouton "Crypter"
                        self.upButtonText.set('  Crypter  ')
                else: #Si la cle actuelle est la dernierre demandee
                    self.keys[self.currentKeyNumber.get()] = self.currentKey.get() # On rajoute la cle actuelle a la liste des cles entrees par l'utilisateur
                    if self.transformChoice.get() == 0: #Si on le bouton radial "Crypter" est selectionne on lance la fonction de cryptage
                        self.result.set(Cryptages.getcryptagebyname(self.choice.get()).crypter(self.cypher.get("1.0", END), self.keys))
                    else: # Sinon on lance la fonction de decryptage
                        self.result.set(Cryptages.getcryptagebyname(self.choice.get()).decrypter(self.cypher.get("1.0", END), self.keys))


            else: # Si aucune cle n'est necessaire alors on lance directement les fonctions de cryptage ou de decryptage
                if self.transformChoice.get() == 0:
                    self.result.set(Cryptages.getcryptagebyname(self.choice.get()).crypter(self.cypher.get("1.0", END), self.keys))
                else:
                    self.result.set(Cryptages.getcryptagebyname(self.choice.get()).decrypter(self.cypher.get("1.0", END), self.keys))

    def downbuttonactions(self): #Fonction executee lors d'un clic sur le bouton inferieur
        if self.choice.get() != '': # On verifie que l'utilisateur est selectionne un cryptage
            if Cryptages.getcryptagebyname(self.choice.get()).iskeyneeded: # On regarde si le cryptage selectionne necessite une cle
                if self.currentKeyNumber.get() == 0: # On regarde si la cle actuelle est la premiere. Si oui, on ne fait rien.
                    return
                else: # La cle actuelle n'est pas la premiere
                    self.currentKeyNumber.set(self.currentKeyNumber.get() - 1) # On retourne a la cle precedente
                    self.keyMessageText.set('Cle ' + str(self.currentKeyNumber.get() + 1) + ' / ' + str(self.keysNumber.get())) # On met a jour l'affichage
                    self.keyInfo.set(self.keysInfos[self.currentKeyNumber.get()].name) # On remet les informations de la cle precedente
                    self.upButtonText.set('  Suivant  ') # On remet le bouton superieur a "Suivant" (Utile uniquement si il etait a "Crypter" ou "Decryper)
                    if self.currentKeyNumber.get() == 0: # Si on est retourne a la premiere cle, on retire le bouton "Precedent"
                        self.downButtonText.set('                 ')
            else: # Si aucune cle n'est necessaire, on ne fait rien
                return

    def infobutton(self): # Fonction executee lors d'un clic sur le bouton d'information sur le cryptage selectionne
        if self.choice.get() != '': # On verifie que l'utilisateur ai bien selectionne un cryptage
            info = Toplevel(width = 300) # On fait apparaitre une nouvelle fenetre ayant une largeur de 300 pixels
            info.title(self.choice.get()) # Le titre de la fenetre est celui du cryptage selectionne

            infoText = Message(info, text = Cryptages.getcryptagebyname(self.choice.get()).getdefinition) # Creer un widget message dont le texte est la definition du cryptage
            infoText.grid(row = 0)

            closeButton = Button(info, text = "Fermer", command = info.destroy) # Bouton pour fermer la fenetre d'informations
            closeButton.grid(row = 1)

    def buttonSet(self): # Fonction executee lors d'un clic sur un des deux boutons radiaux
        if self.choice.get() != '': # On verifie que l'utilsateur est selectionne
            # Si une cle est necessaire, que la cle actuelle est la derniere cle et si l'utilisateur a cliquer sur le bouton radial "Crypter" alors le bouton superieur affiche "Crypter"
            if self.transformChoice.get() == 0 and (Cryptages.getcryptagebyname(self.choice.get()).iskeyneeded == False or (self.currentKeyNumber.get() + 1)== self.keysNumber.get()):
                self.upButtonText.set('  Crypter  ')
            # Si une cle est necessaire, que la cle actuelle est la derniere cle et si l'utilisateur a cliquer sur le bouton radial "Decrypter" alors le bouton superieur affiche "Decrypter"
            if self.transformChoice.get() == 1 and (Cryptages.getcryptagebyname(self.choice.get()).iskeyneeded == False or (self.currentKeyNumber.get() + 1) == self.keysNumber.get()):
                self.upButtonText.set(' Décrypter ')

interface = Interface(window)
interface.mainloop()
