from enum import Enum

import lib_cryptage


class Type(Enum):
    alphanumeric = ("an")
    alphabetic = ("a")
    numeric = ("n")
    special = ("s")

    def __init__(self, _type):
        self.Type = _type

    def conforme(self, string):
        if self.Type == 's':
            return True
        else:
            if self.Type == 'n':
                try:
                    temp = int(string)
                    return True
                except:
                    return False

            for char in string:
                if char.isdigit() or char.isalpha() or char == ' ' or char == '\ ' or char == '\\':

                    if self.Type == 'a':
                        if char.isdigit():
                            return False
                else:
                    return False
            return True

    @property
    def type(self):
        if self.Type == 'a':
            return 'alphabetic'
        elif self.Type == 'an':
            return 'alphanumeric'
        elif self.Type == 'n':
            return 'numeric'
        else:
            return 'special'

    @property
    def name(self):
        return self.type


def testTypes():
    for type in Type:
        print(type.type)
        print("\'5\' is " + ('' if type.conforme('5') else 'NOT') + " conforme")
        print("\' \' is " + ('' if type.conforme(' ') else 'NOT') + " conforme")
        print("\'a\' is " + ('' if type.conforme('a') else 'NOT') + " conforme")
        print("\'&\' is " + ('' if type.conforme('&') else 'NOT') + " conforme")


class Key():
    Type = Type.special
    name = 'key'
    info = 'definition'

    def __init__(self, _name, _type, _info):
        self.name = _name
        self.Type = _type
        self.info = _info

    def conforme(self, string):
        return self.Type.conforme(string)

    @property
    def type(self):
        return self.Type.name


def testkeys():
    for type in Type:
        key = Key(type.name, type, "clé test de type " + type.name)
        print("test de clé yolo de type " + key.type)
        print("\'axolot\' is " + ('' if key.conforme('axolot') else 'NOT') + " conforme")
        print("\'ax0lot\' is " + ('' if key.conforme('ax0lot') else 'NOT') + " conforme")
        print("\'480107\' is " + ('' if key.conforme('480107') else 'NOT') + " conforme")
        print("\'A&ol0t\' is " + ('' if key.conforme('A&ol0t') else 'NOT') + " conforme \n")


class Cryptage(Enum):
    cesar = ("Chiffre de César", Type.special, True,
             "En cryptographie, le chiffrement par décalage, aussi connu comme le chiffre de César , est une méthode de chiffrement très simple utilisée par Jules César dans ses correspondances secrètes (ce qui explique le nom Â« chiffre de César Â»). Le texte chiffré s\'obtient en remplaçant chaque lettre du texte clair original par une lettre à distance fixe, toujours du même cÃ´té, dans l\'ordre de l\'alphabet. Pour les dernières lettres (dans le cas d'un décalage à droite), on reprend au début.",
             [Key("Décalage dans l\'alphabet", Type.numeric, "Décalage dans l\'alphabet au debut du codage")])

    che = ("Chiffre de Ché", Type.special, False,
           "Ernesto Rafael Guevara de la Serna alias 'Che Guevara' était un révolutionnaire marxiste d\'Amérique latine, né le 14 juin 1928 à Rosario et décédé le 8 octobre 1967 à La Higuera en Bolivie. Plus précisément, il fut exécuté par l\'armée bolivienne, et on retrouva sur son corps un papier indiquant le procédé qu\'il utilisait pour échanger des messages avec Fidel Castro. Les lettres sont substituées par des nombres qui sont ensuite regroupées en groupes au nombre fixe de chiffres, ceux ci vont alors etre additionnés modulo 10 (on ne prends pas en compte la retenue, ex: 6+6 = 2). Ce cryptage est en fait un systeme de vernam (la taille de la clé est la meme que celle du message)")

    vigenere = ("Babbage et code de vigenère", Type.special, True,
                "Le chiffre de Vigenère est un système de chiffrement polyalphabétique, c'est un chiffrement par substitution, mais une même lettre du message clair peut, suivant sa position dans celui-ci, être remplacée par des lettres différentes, contrairement à un système de chiffrement monoalphabétique comme le chiffre de César (qu\â€˜il utilise cependant comme composant). Cette méthode résiste ainsi à l\â€˜analyse de fréquences, ce qui est un avantage décisif sur les chiffrements monoalphabétiques. Cependant le chiffre de Vigenère a été cassé par le major prussien Friedrich Kasiski qui a publié sa méthode en 1863. Il n\â€˜offre plus depuis cette époque aucune sécurité.",
                [Key("Clé de chiffrement", Type.alphabetic,
                     "Clé utilisée pour le chiffrement du message, elle servira a determiner le caractére codé.")])

    vernam = ("Vernam, ou le téléphone rouge",
              Type.special,
              True,
              """En 1917, Gilbert Vernam mit au point un algorithme de chiffrement -basé sur une clé secrète- parfaitement sûr, tellement sûr qu'il a longtemps protégé le fameux 'téléphone rouge', qui reliait la Maison Blanche au Kremlin. Ce système, bien qu'apportant une sécurité optimale, est très peu d'usage dans le monde civil, et est surtout réservé à des organismes possédant des moyens importants. En effet, tout le monde n'est pas en mesure d'utiliser des clés de la manière décrite ci dessous:

    Générer des clés gigantesques (puisqu'au moins de la taille de l'ensemble des messages à envoyer) nécessite une grande puissance de calcul
    Transporter de telles clés, dont le secret doit être maintenu à tout prix, n'est pas chose aisée : tout le monde ne dispose pas de la valise diplomatique...
""",
              [Key("clé", Type.special,
                   """Pour une sécurité inconditionnelle (théoriquement inviolable), cette clé doit respecter ces trois règles:
    La clé doit être de la taille du message à envoyer.
    Les lettres de cette clé doivent être choisies de façon totalement aléatoire.
    La clé ne doit servir qu'une seule et unique fois.""")])

    rsa = ("RSA ou clé publique-clé privée", Type.special, False,
           "Pour des raisons pratiques et de performances, nous utiliserons des clés prédéfinies. L\'algorithme RSA (du nom de ses inventeurs Ron Rivest, Adi Shamir et Len Aldeman, qui ont imaginé le principe en 1978) est utilisé pour la cryptographie à clé publique et est basé sur le fait qu\'il est facile de multiplier deux grands nombres premiers mais difficile de factoriser le produit. Câ€™est lâ€™exemple le plus courant de cryptographie asymétrique, toujours considéré comme sûr, avec la technologie actuelle, pour des clés suffisamment grosses (1024, 2048 voire 4096 bits).")

    alberti = ("Cadran chiffrant d\'alberti", Type.special, True,
               "Composé de deux cadrans superposés dont le plus grand est fixe et le plus petit par dessus, mobile, le cadran chiffrant d\'alberti est le premier cryptage polyalphabetique (utilisant des alphabets differents). Pour s\'en servir, il faut trouver la lettre cherchée sur le grand cadrant et ecrire celle située dessous (petit cadrant) et tourner le cadran d\'un nombre voulu de cran par la suite, et ainsi de suite.",
               [Key("décalage", Type.numeric,
                    "Sert de decalage de base entre la position du grand alphabet et du petit"),
                Key("incrémentation", Type.numeric,
                    "C\'est le décalage que l\'on ajoutera entre les deux alphabets a chaque période atteinte."),
                Key("periode", Type.numeric,
                    "C\'est le nombre de caractere codés avant d\'incrémenter le décalage entre les deux alphabets."),
                Key("alphabet grand cadran", Type.special,
                    "L\'alphabet utilisé sur le cadrant codant (les caracteres qui seront codés par le cryptage"),
                Key("alphabet petit cadran", Type.special,
                    "Alphabet codé qui permet de determiner le caractère chiffré, doit etre de meme taille que l\'alphabet du grand cadran")])

    braintricks = ("Brain tricks (cryptage original)", Type.special, False,
                   "Ce petit cryptage amusant que nous avons créé a pour but de jouer avec les capacités de votre cerveau en mettant dans le desordre les lettres a l\'interieur de chaque mot. (premiere et derniere lettres exclues) Il permet de voir que meme desordonnés les mots sont aussi lisible que ordonnés et notre cerveau peut lire le texte sans aucun probleme de comprehension.")

    sandmusset = ("Le code G.Sand - Musset", Type.special, False,
                  "Parmi les lettres les plus connues de la langue française on retrouve de superbes exemples de stéganographie (dissimulation de l\'information), surtout dans les lettres échangées par Georges Sand et Alfred de Musset. En effet, pour decouvrir le message caché dans ce nuage de phrase, il suffit de n\'en lire qu\'une sur deux!")

    def __init__(self, Name, CypherType, KeyNeeded, Definition, _keys=[]):
        self.cyphertype = CypherType
        self.cryptageName = Name
        self.isKeyNeeded = KeyNeeded
        self.definition = Definition

        self.keys = _keys
        self.keyCount = len(_keys)

    def verification(self, message, keys=[]):
        if len(message) == 0:
            return "Le cypher est vide!"
        # verification que le cypher est du bon type
        if not self.cyphertype.conforme(message):
            return "Le cypher n'est pas conforme au format " + self.cyphertype.name + "!"
        # verification que les clés sont du bon type
        if self.iskeyneeded:
            if self.keyCount > 1:
                ikey = 0
                iarg = 0
                for i in range(self.keyCount):
                    key = self.keys[ikey]
                    arg = keys[iarg]
                    if not key.conforme(arg):
                        return key.name + " n'est pas conforme au format " + key.type + "!"
                ikey += 1
                iarg += 1
            else:
                key = self.keys[0]
                if not key.conforme(keys[0]):
                    return key.name + " n'est pas conforme au format " + key.type + "!"
        method = getattr(lib_cryptage, "verif_" + self.name)
        return method(message, keys)

    def decrypter(self, message, keys=[]):
        verif = self.verification(message, keys)
        if verif != "":
            return verif

        method = getattr(lib_cryptage, "de_" + self.name)
        if self.isKeyNeeded:
            result = method(message, keys)
        else:
            result = method(message)
        return result

    def crypter(self, message, keys=[]):
        verif = self.verification(message, keys)
        if verif != "":
            return verif

        method = getattr(lib_cryptage, "cr_" + self.name)
        if self.isKeyNeeded:
            result = method(message, keys)
        else:
            result = method(message)
        return result

    @property
    def iscypherconforme(self, cypher):
        return self.cyphertype.conforme(cypher)

    @property
    def getkeycount(self):
        return self.keyCount

    @property
    def getkeys(self):
        return self.keys

    @property
    def getcryptagename(self):
        return self.cryptageName

    @property
    def iskeyneeded(self):
        return self.isKeyNeeded

    @property
    def getdefinition(self):
        return self.definition

    @property
    def getInfos(self):
        message = ""
        message = "".join([message, "Cryptage name: ", self.cryptageName, "\n"])
        message = "".join(
            [message, "A key is ", "NOT " if not self.isKeyNeeded else "", "needed to use this encryption.\n"])

        if self.isKeyNeeded:
            message = "".join([message, "keys required: \n"])
            count = 1
            for key in self.keys:
                message = "".join([message, str(count), ". ", key.name, " (", key.type, ") : " + key.info + "\n"])
                count += 1

        message = "".join([message, "More infos:\n\t", self.definition, "\n\n"])
        return message


def getNames():
    namelist = []
    for enum in Cryptage:
        namelist.append(enum.cryptageName)
    return namelist


def getcryptagebyname(name):
    for item in Cryptage:
        if item.getcryptagename == name:
            return item


def testEnum():
    for enum in Cryptage:
        print(enum.getInfos)


def debugalberti():
    alphabetext = "abcdefgiklmnopqrstuvxyz1234"
    alphabetint = "azertyuiopqsdfghjklmwxcvbn&"
    decay = "41"
    increment = 6
    periode = "2b"
    message = "yolo swag test 3245687 !!!:;,"

    code = Cryptage.alberti.crypter(message, [decay,
                                              increment,
                                              periode,
                                              alphabetext,
                                              alphabetint])
    print(message, "codé via alberti, cela donne:\n " + code)
    print("Decode avec les memes arguments cela donne:\n " + Cryptage.alberti.decrypter(code, [decay,
                                                                                               increment,
                                                                                               periode,
                                                                                               alphabetext,
                                                                                               alphabetint]))
