# on importe tkinter pour l'interface
from tkinter import *
#on importe le moteur
import Cryptages

# on crée une fenetre nommée Cryptotron
window = Tk()
window.title("Cryptotron")

#on crée une classe dérivée de Frame pour l'interface.
class Interface(Frame):
    """actualcryptage contient le cryptage selectionné ou 0 si aucun ne l'est."""
    actualcryptage = 0

    """keyzonelist contient les zones d'entrée des clés"""
    keyzonelist = []

    """Cette fonction est le constructeur de la classe interface, permettant d'en créer
    une instance et de s'en servir."""
    def __init__(self, window):
        #on crée l'interface à une taille minimale ridicule pour eviter
        # des problemes de remplissage de grille avec les éléments.
        window.minsize(width=1, height=1)
        Frame.__init__(self, window, width=2, height=2)
        self.grid()

        ##zone du menu et des descriptions sur les cryptages
        # on ajoute une zone menu a l'interface, avec un decalage horizontal de 4 px
        self.menuFrame = Frame(window)
        self.menuFrame.grid(row=0, column=0, padx = 4)

        ##nom de la zone
        # on ajoute le nom de la zone dans la zone menu, tout en haut de celle ci et centrée
        Label(self.menuFrame, text = "Menu").grid(row = 0, sticky = W+E)

        ##menu
        # on ajoute le menu a sa zone, en dessous du titre et ajusté a la zone
        self.menu = Listbox(self.menuFrame)
        self.menu.grid(row = 1,sticky = N+E+S+W)
        # on ajoute l'evenement selection qui appellera la fonction onselect quand on cliquera sur un élément de la liste
        self.menu.bind('<<ListboxSelect>>', self.onselect)
        # on initialize les éléments du menu
        self.initializemenu()

        ##zone histoire du cryptage
        # on ajoute la zone infos cryptage dans la zone menu et la plaçons sous le menu
        self.menuinfosFrame = Frame(self.menuFrame)
        self.menuinfosFrame.grid(row=2)

        ##nom de la zone
        # on affiche le titre de la zone infos en haut de celle ci et centré
        Label(self.menuinfosFrame, text = "Son histoire").grid(row = 0, sticky = W+E)
        
        ##histoire du cryptage
        # On affiche une zone de texte destinée aux infos du cryptage
        self.menuinfos = Text(self.menuinfosFrame,  # dans la zone info du menu
                              wrap=WORD,  # revenant a la ligne a chaque mot trop grand pour rentrer dans une ligne
                              height = 10,  # de hauteur 10 pixels
                              width = 30,  # de largeur 30 pixels
                              state = DISABLED)  # et verrouillé pour eviter les modifications par l'utilisateur
        # on le place sous le titre de la zone et l'ajuste a la taille de celle ci
        self.menuinfos.grid(row = 1, sticky = N+S+E+W)

        ##scrollbar histoire cryptage
        # on ajoute une barre de défilement vertical à la zone d'infos
        self.infoscrollbar = Scrollbar(self.menuinfosFrame,
                                       command=self.menuinfos.yview)  #recupérant la position verticale de la vue a chaque interaction 
        # on lie le défilement de la zone d'infos à la barre de défilement
        self.menuinfos.configure(yscrollcommand=self.infoscrollbar.set)
        #on l'affiche à la gauche de la zone de texte et l'ajuste verticalement pour faire la taille de la zone de texte
        self.infoscrollbar.grid(row = 1, column = 1 , sticky = E+N+S)

        #aide supplementaire sur cryptage choisi
        #on crée un bouton aide
        Button(self.menuinfosFrame,  #dans la zone infos
               command=self.aide,  # et appellant la fonction aide pour faire apparaitre une popup a chaque clic
               text = " ? ").grid(row=2,column=0) # et affichant un point d'interrogation, on la place sous la zone de texte, centrée.

        ##zone d'entree du texte et du resultat
        #on crée la zone de dialogue avec l'utilisateur
        self.dialogFrame = Frame(window)
        # et la plaçons dans l'interface a gauche de la zone de menu,
        # avec un décalage vertical de 5 et horizontal de 4
        self.dialogFrame.grid(row=0, column=1, padx = 4, pady = 5)

        ##zone du texte
        #on crée la zone d'entrée du texte a chiffrer ( le cypher )
        self.cypherFrame = Frame(self.dialogFrame)
        #et la plaçons dans la zone de dialogue
        self.cypherFrame.grid(row=0, column=0)

        ##nom de la zone
        # on affiche le nom de la zone de cypher dedans
        Label(self.cypherFrame, text="Cypher (texte à transformer)").grid(row = 0, column = 0, sticky = W+E)

        ##entrée du texte
        # on ajoute l'entrée de texte
        self.cypher = Text(self.cypherFrame, # dans la zone du cypher
                           wrap=WORD,  # avec retour a la ligne pour les phrases trop longues
                           width=40,  # de largeur 40 pixels
                           height=10)  # de hauteur 10 pixels
        # on affiche l'entrée en dessous du titre de la zone et l'ajustons verticalement et vers la gauche
        self.cypher.grid(row=1, column=0, sticky = W+N+S)

        ##scrollbar de l'entrée de texte
        #on ajoute la barre de défilement vertical de la zone d'entrée
        # de la meme manière qu'au dessus
        self.cypherscrollbar = Scrollbar(self.cypherFrame, command=self.cypher.yview)
        self.cypher.configure(yscrollcommand=self.cypherscrollbar.set)
        self.cypherscrollbar.grid(row = 1, column = 1 , sticky = E+N+S)

        ##zone de resultat
        #on crée la zone de résultat que l'n place en dessous de la zone d'entrée dans la zone de dialogue.
        self.resultFrame = Frame(self.dialogFrame)
        self.resultFrame.grid(row=1, column=0)

        ##nom de la zone
        # on affiche le nom de la zone
        Label(self.resultFrame, text="Resultat (texte transformé)").grid(row = 0, column = 0, sticky = W+E)
        
        ##affichage resultat
        # on crée l'affichage du résultat sous forme de zone de texte pour pouvoir
        # defiler les longs textes sans agrandir la fenetre et verouillons la zone pour
        # eviter que l'utilisateur y écrive ce qu'il veut.
        self.result = Text(self.resultFrame, wrap=WORD, width=40, height=15, state=DISABLED)
        self.result.grid(row=1, column=0, sticky = W+N+S)

        ##scrollbar de l'affichage resultat
        # on crée de nouveau une barre de filement comme au dessus pour la zone de resultat
        self.resultscrollbar = Scrollbar(self.resultFrame, command=self.result.yview)
        self.result.configure(yscrollcommand=self.resultscrollbar.set)
        self.resultscrollbar.grid(row = 1, column = 1, sticky = E+N+S)

        ##zone clés et actions
        # on crée la zone des arguments,
        # la zone des clés et des boutons actions.
        # on ajoute cette zone a la droite des zones de menu et dialogue
        self.argsFrame = Frame(window)
        self.argsFrame.grid(row=0, column=2, sticky=W+E)

        ##zone des clés
        # on ajoute la zone des clés en haut de la zone d'arguments
        self.keyFrame = Frame(self.argsFrame)
        self.keyFrame.grid(row=0, column=0, sticky=N+S)

        ##zone des boutons d'action
        # on ajoute la zone des boutons en dessous de la zone des clés
        self.buttonFrame = Frame(self.argsFrame)
        self.buttonFrame.grid(row=1, column=0, sticky=N+S)

        ##bouton convertir
        #on ajoute le bouton convertir dans cette zone, qui apellera crypter() quand on cliquera dessus
        self.convertButton = Button(self.buttonFrame,
                                    text="Convertir",
                                    command=self.crypter)
        self.convertButton.grid(row=0, column=0, sticky = N)

        ##bouton deconvertir
        #on ajoute le bouton decrypter à sa droite, qui apellera decrypter() quand on cliquera dessus
        self.decryptButton = Button(self.buttonFrame, text="Decrypter", command=self.decrypter)
        self.decryptButton.grid(row=0, column=1, sticky = N)

        ##bouton inverser
        #on ajoute le bouton inverser en dessous, qui apellera inverser() quand on cliquera dessus
        self.invertButton = Button(self.buttonFrame, text="Inverser", command=self.inverser)
        self.invertButton.grid(row=1, column=0, columnspan = 1,  sticky = W+E)

    """Cette fonction initialise le menu en y ajoutant tous les cryptages disponibles"""    
    def initializemenu(self):
        #pour chaque cryptage dans l'enumeration de Cryptage
        for item in Cryptages.Cryptage:
            #on ajoute un objet au menu ayant pour nom le nom de cryptage
            self.menu.insert(END, item.getcryptagename)
        return
    
    """Cette fonction permet d'inverser le contenu de la zone de Cypher et de la zone de résultats."""
    def inverser(self):
        #on recupere le contenu des zones de récultat et de cypher
        textcypher = self.cypher.get("1.0", END)
        textresult = self.result.get("1.0", END)
        #on vide la zone de cypher et y insère le contenu de la zone de résultat
        self.cypher.delete("1.0",END)
        self.cypher.insert("1.0", textresult)
        #on déverouille la zone de résultat
        self.result.configure(state=NORMAL)
        #on la vide et y insère le contenu stocké de la zone cypher 
        self.result.delete("1.0",END)
        self.result.insert("1.0", textcypher)
        #on reverouille la zone de résultat
        self.result.configure(state=DISABLED)
        return

    """Cette fonction permet de créer et afficher la popup d'aide"""
    def popup(self, titre, texte):
        #la popup  est créee
        popup = Tk()
        # et son titre est celui passé en argument
        popup.wm_title(titre)
        #La zone de texte
        Label(popup,  #s'affiche dans la popup
              text=texte,  #le texte a afficher est celui passé en argument
              wraplength=350,  # a chaque fois que la flongueur du texte passe à 350 pixels, on reviens à la ligne
              anchor=W,  # le texte doit se situer sur la gauche de la zone
              padx=15,  # la zone doit avoir une marge horizontale de 15 pixels
              pady=10,  # ainsi qu'une zone verticale de 10 pixels
              justify=LEFT).grid(sticky=N+E+W+S, pady=10)  #elle est alors affichée et centrée dans la popup
        #Le bouton Quitter
        Button(popup,  #s'affiche dans la popup
               text="Okay",  # le bouton affiche Okay
               command = popup.destroy).grid() # et a pour interaction de fermer la popup
        #on lance la popup prête.
        popup.mainloop()

    """ Cette fonction sert à lancer la création de la
    pop up d'informations sur le crytage selectionné"""
    def aide(self):
        # pour afficher la popup, il nous faut un cryptage de sélectionné
        if isinstance(self.actualcryptage, Cryptages.Cryptage):
            # si la sélection est un cryptage
            # ou autrement si un cryptage est selectionné
            # on recupere celui ci
            crypt = self.actualcryptage
            # on ouvre une popup contenant ses informations
            self.popup(crypt.getcryptagename,crypt.getInfos)
        else:
            #sinon on affiche à l'utilisateur qu'il n'en a pas encore sélectionné un.
            self.popup("ATTENTION","Choisissez un cryptage pour avoir des informations \nconcernant ses clés et son mode de fonctionnement.")
        
    """Cette fonction mets à jour la zone d'entrée des clés
    nécessaires au cryptage selectionné"""
    def updatekeyframe(self, newcrypt):
        #on va d'abord nettoyer la zone des clés
        for zonekey in self.keyzonelist:
            #pour chaque zone de clés
            # on recupere la zone nom de la clé
            keylabel = zonekey[0]
            # on recupere la zone entrée de la clé
            keyentry = zonekey[1]
            # on retire les deux zones de l'interface 
            keylabel.grid_remove()
            keyentry.grid_remove()
            # on supprime definitivement ces zones
            del keylabel
            del keyentry
        # on vide totalement la liste de zone de clés
        self.keyzonelist = []

        #on commence tout en haut
        _row = 0
        for key in newcrypt.getkeys:
            # pour chaque clé du cryptage actuel
            # on crée une zone nom de la clé
            keylabel = Label(self.keyFrame, text=key.name + ": ")
            # on crée une zone d'entrée de la clé
            keyentry = Entry(self.keyFrame)
            # on affiche les deux zones l'une a coté de l'autre
            keylabel.grid(row=_row, column=0, sticky=S+N+W)
            keyentry.grid(row=_row, column=1, sticky=S+N+E)
            # on ajoute la combianaison de ces deux zones dans un tableau
            keyzone = [keylabel, keyentry]
            # on ajoute ce tableau a la liste des zone de clés
            self.keyzonelist.append(keyzone)
            # on descend d'une ligne
            _row += 1
        return
    
    """Cette fonction permet de mettre à jour les informations basiques sur le cryptage actuel"""
    def updatecryptinfos(self, newcrypt):
        # on dévérouille la zone d'informations
        self.menuinfos.configure(state=NORMAL)
        # on supprime son contenu
        self.menuinfos.delete("1.0", END)
        # on y insere la definition du cryptage actuel
        self.menuinfos.insert("1.0", newcrypt.getdefinition)
        # on reverrouille la zone.
        self.menuinfos.configure(state=DISABLED)

    """Cette fonction lance les mises a jours des éléments de la fenêtre lors d'un changement
    de choix dans le menu"""    
    def onselect(self, event):
        # on recupere le menu
        w = event.widget
        # on recupere le dernier élément sélectionné par la souris dans le menu
        index = int(w.curselection()[0])
        # on recupere le cryptage au nom correspondant à celui de cet élément
        crypt = Cryptages.getcryptagebyname(w.get(index))
        #on verifie si ce nouveau cryptage est different de l'actuel
        if isinstance(self.actualcryptage, Cryptages.Cryptage):
            #Si le cryptage stocké est un cryptage (il ne l'est pas à l'initialisation de l'interface)
            #donc Si unn cryptage a deja été choisi avant
            if crypt.name == self.actualcryptage.name:
                #Si ce cryptage est identique au nouveau
                #on ne fait rien
                return
        # sinon
        # on mets a jour la zone des clés
        self.updatekeyframe(crypt)
        # on mets à jour les informations sur le cryptage
        self.updatecryptinfos(crypt)
        # on stocke le nouveau cryptage selectionné.
        self.actualcryptage = crypt

    """Cette fonction récupere les clés et le cypher et écris le résultat de la
    tentative de cryptage. (écris soit le résultat soit le message d'erreur en cas
    de non-conformité des clés nécessaires)"""
    def crypter(self):
        #on deverrouille la zone de résultats
        self.result.configure(state=NORMAL)
        #on supprime son contenu
        self.result.delete("1.0", END)
        # on recupere le cypher 
        cypher = self.cypher.get("1.0", END)
        # on recupere le cryptage actuel
        crypt = self.actualcryptage
        #on crée une liste pour contenir les clés
        keys = []
        for keyzone in self.keyzonelist:
            #pour chaque zone de clé dans la liste de zone de clé
            #la valeur de la clé est recupérée (keyzone[1] = Entrée de la clé, keyzone[0] = nom de la clé)
            keyvalue = keyzone[1].get()
            #on ajoute cette clé a la liste
            keys.append(keyvalue)
        #on affiche le resultat de la tentative de cryptage
        self.result.insert(END, crypt.crypter(cypher, keys))
        # on verrouille de nouveau la zone de résultats
        self.result.configure(state=DISABLED)

    """Cette fonction récupere les clés et le cypher et écris le résultat de la
    tentative de décryptage. (écris soit le résultat soit le message d'erreur en cas
    de non-conformité des clés nécessaires)"""
    def decrypter(self):
        #on deverrouille la zone de résultats
        self.result.configure(state=NORMAL)
        #on supprime son contenu
        self.result.delete("1.0", END)
        # on recupere le cypher 
        cypher = self.cypher.get("1.0", END)
        # on recupere le cryptage actuel
        crypt = self.actualcryptage
        #on crée une liste pour contenir les clés
        keys = []
        for keyzone in self.keyzonelist:
            #pour chaque zone de clé dans la liste de zone de clé
            #la valeur de la clé est recupérée (keyzone[1] = Entrée de la clé, keyzone[0] = nom de la clé)
            keyvalue = keyzone[1].get()
            #on ajoute cette clé a la liste
            keys.append(keyvalue)
        #on affiche le resultat de la tentative de décryptage
        self.result.insert(END, crypt.decrypter(cypher, keys))
        # on verrouille de nouveau la zone de résultats
        self.result.configure(state=DISABLED)

# on crée l'interface
interface = Interface(window)
# on lance l'interface
interface.mainloop()


