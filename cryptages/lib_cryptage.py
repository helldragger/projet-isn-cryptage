########CHE########


"""Fonction de verification specifique pour la methode de che, pas de tests compleementaires"""


def verif_che(message, keys):
    # pas de cle a verifier
    return ""


"""Ces deux dictionnaires contiennent le code utilise par Che pour chaque caractère et inversement."""
c_che = {"A": 6, "B": 38, "C": 32, "D": 4, "E": 8, "F": 30, "G": 36, "H": 34, "I": 39, "J": 31, "K": 78, "L": 72,
         "M": 70, "N": 76, "O": 9, "P": 79, "Q": 71, "R": 58, "S": 2, "T": 0, "U": 52, "V": 50, "W": 56, "X": 54,
         "Y": 1, "Z": 59}
d_che = {6: "A", 38: "B", 32: "C", 4: "D", 8: "E", 30: "F", 36: "G", 34: "H", 39: "I", 31: "J", 78: "K", 72: "L",
         70: "M", 76: "N", 9: "O", 79: "P", 71: "Q", 58: "R", 2: "S", 0: "T", 52: "U", 50: "V", 56: "W", 54: "X",
         1: "Y", 59: "Z"}


def de_che(code):
    # on recupere le dictionnaire fait pour le dechiffrage
    global d_che
    result = ""
    # chars va servir a garder en memoire les chiffres jusqu'à retrouver les nombres correpspondants et les convertir
    chars = ""
    for char in code:
        # pour chaque caractère dans le code
        if not char.isdigit():
            # si ce n'est pas un chiffre
            # on l'ajoute directement au mossage decrypte
            result = "".join([result, char])
            # et on passe au suivant
            continue
        # sinon on garde en memoire
        chars = chars + char
        try:
            # si le nombre reconstitue est reconnu par le dictionnaire
            # alors on ajoute son caractère correspondant a la suite du message dechiffre
            result = "".join([result, d_che[int(chars)]])
            # et on vide le nombre en memoire
            chars = ""
        except:
            # sinon on ne change rien
            chars = chars
    # retourne le resultat.
    return result


def cr_che(code):
    # on mets le message en majuscules
    code = code.upper()
    global c_che
    result = ""
    for char in code:
        # pour chaque caractère du message
        if (char.isalpha()):
            # si c'est une lettre
            # on ajoute au resultat le nombre correspondant a la lettre
            result = "".join([result, str(c_che[char])])
        elif (char.isdigit()):
            # si c'est un chiffre, on passe et ne le mettons pas dans le message
            continue
        else:
            # Si c'est un caractère special, on lajoute dans le resultat tel quel
            result = "".join([result, char])
            continue
    return result

########CESAR########
"""Fonction de verfication specifique pour la methode de cesar, pas de tests compleementaires"""


def verif_cesar(message, keys):
    # rien d'autre a verifier
    return ""


"""Cette fonction permet de recuperer la lettre correspondante au decalage cle"""


def getchar(char, cle):
    if char.isalpha():
        # si le caractère est une lettre
        # pour eviter que l'ordonnee depasse ou sois en dessous de celui de l'alphabet
        # on va reprendre la valeur % 26 (sa place dans l'alphabet)
        # et l'ajouter a l'ordonnee de depart de l'alphabet (selon s'il est en majuscule ou en minuscule)
        # le rang dans l'alphabet est egal a (ord(char) - ord(plus petite lettre))
        # celle code est egal a rang + cle % 26
        if char.isupper():

            val = ((ord(char) - ord("A")) + cle) % 26
            # s'il est en majuscule alors val = ord de A + place dans l'alphabet
            val = ord("A") + val
        else:
            val = ((ord(char) - ord("a")) + cle) % 26
            # s'il est en minuscule alors val = ord de a + place dans l'alphabet
            val = ord("a") + val
        # on retourne alors la lettre correspondant à l'ordonnee.

        return chr(val)
    # sionon on retourne le caractere directement
    return char


def de_cesar(code, keys):
    # on recupere la cle
    cle = int(keys[0])

    message = ""
    for char in code:
        # pour chaque caractere dans le message,
        # on recupere le caractère decode get char( code, -cle)
        nwchar = getchar(char, -cle)
        # on l'ajoute au message decode
        message = "".join([message, nwchar])
    # on renvoie le tout
    return message


def cr_cesar(message, keys):
    # on recupere la cle
    cle = int(keys[0])

    code = ""
    for char in message:
        # pour chaque caractere dans le message,
        # on recupere le caractère code get char( clair, cle)
        nwchar = getchar(char, cle)
        code = "".join([code, nwchar])
    # on retourne le tout
    return code

########VIGENERE########
"""Fonction de verfication specifique pour la methode de vigenère, pas de tests compleementaires"""


def verif_vigenere(message, keys):
    # rien d'autre a verifier
    return ""


def de_vigenere(code, keys):
    message = ""
    key = keys[0]
    key = key.upper()  # on mets la cle en majuscules
    code = code.upper()  # onmets le mssage en majuscules
    ikey = 0  # l'index du caractere actuel dans la cle de chiffrement
    for char in code:  # pour chaque caractere du message code
        decode = char  # on garde en memoire le caractere
        if char.isalpha():  # si c'est une lettre
            # on recupere le rang de la lettre codée ord(char) - ord(key[ikey]),
            # qu'on reporte sur l'alphabet %26,
            # et on ajoute le rang zero (ord('A'))
            rangcode = ((ord(char) - ord(key[ikey])) % 26) + ord('A')  # on decode le rang de la lettre cryptee
            # on le reporte depuis la fin de l'alphabet ( % 26)

            decode = chr(rangcode)  # on recupere le charactere correspondant à ce rang
            ikey += 1  # on avance d'un caractere dans la cle
            ikey %= len(key)  # on repart du debut
        message = "".join([message, decode])  # on ajoute le caractere decode au message decrypte
    return message


def cr_vigenere(message, keys):
    code = ""
    key = keys[0]
    key = key.upper()
    message = message.upper()
    ikey = 0  # l'index du caractere actuel dans la cle de chiffrement
    for char in message:  # pour chaque caractere du message à coder
        newchar = char  # on garde en memoire le caractere
        if char.isalpha():  # si c'est une lettre

            # on recupere le rang de la lettre claire ord(char) + ord(key[ikey]),
            # qu'on reporte sur l'alphabet %26,
            # et on ajoute le rang zero (ord('A'))
            rangCode = ((ord(char) + ord(key[ikey])) % 26) + ord('A')  # on code le rang de la lettre à crypter
            # on le reporte depuis la debut de l'alphabet ( % 26 )

            newchar = chr(rangCode)  # on recupere le charactere correspondant à ce rang
            ikey += 1  # on avance d'un caractere dans la cle
            ikey %= len(key)  # si on a utilise la cle jusqu'au bout
            # on repart du debut
        code = "".join([code, newchar])  # on ajoute le caractere decode au message code
    return code

########SAND-MUSSET########
"""Fonction de verfication specifique pour la methode de sand-musset, pas de tests compleementaires"""


def verif_sandmusset(message, keys):
    # pas de cles a verifier
    return ""


def de_sandmusset(message):
    result = ''
    resultarray = message.split('\n')[::2]  # on recupere une ligne sur deux du message
    for line in resultarray:
        # on met le tout en forme pour etre affiche
        result = ''.join([result, '\n', line])
    return result


def cr_sandmusset(*args):
    return "Un logiciel ne peut pas deguiser un texte en en imaginant un autre pour vous,\n vous devez vous en occuper personnellement."

########BRAINTRICKS########
"""Fonction de verfication specifique pour la methode de braintricks, pas de tests compleementaires"""


def verif_braintricks(message, keys):
    # pas de cles a verifier
    return ""


"""Ceci est une fonction pratique pour transformer un tablleau de valeurs en string"""


def tostring(array):
    string = ""
    for char in array:
        string += str(char)
    return string


"""Ceci est une fonction pratique pour remplacer tout caractère non alphabetique a la fois dans une string en un appel"""


def replaceall(message):
    temp = ""
    for char in message:
        # poru chaque caractère du message
        if ord('a') <= ord(char) <= ord('z') \
                or ord('A') <= ord(char) <= ord('Z') \
                or char == " " \
                or char.isdigit():
            # s'il est une lettre, un espace ou un chiffre, on l'ajoute.
            temp = "".join([temp, char])
    # on  retourne le message final
    return temp


def cr_braintricks(message):
    # on importe random
    import random

    # on remplace tout caractère non alphanumerique en rien
    message = replaceall(message)
    # on recupere les mots separes par des uniques espaces
    words = message.split(' ')
    code = ""
    for word in words:
        # pour chaque mot
        if word == "":
            # s'il est vide, on passe
            continue
        if len(word) > 2:
            # s'il contient plus de deux caractères,
            # on stocke le premier et dernier caractère
            avant = word[0]
            apres = word[-1]
            # on recupere les lettres entre les deux extremites
            taille = len(word)
            temp_word = list(word[1:taille - 1:])
            # on les melange aleatoirement
            random.shuffle(temp_word)
            # on reconstitue le mot avec sa premiere lettre, le melange et la derniere lettre
            # on l'ajoute au code
            code += avant + tostring(temp_word) + apres + " "

        else:
            # sinon s'il fait en tre 1 et 2 caractères, on l'ajoute directement au code
            code += tostring(word) + " "
    # on renvoie le reste.
    return code

######ALBERTI######
"""Fonction de verfication specifique pour la methode de alberti,
Il faut que les deux alphabets soient de meme longueur et que le message
ne contienne pas de caractère non present dans l'un ou dans l'autre"""


def verif_alberti(message, keys):
    # verification que les deux alphabets aie la meme taille
    if len(keys[3]) != len(keys[4]):
        return "Les deux alphabets sont de longueur differentes! (" + str(
            len(keys[3])) + " sur l'exterieur contre " + str(len(
            keys[4])) + " sur l'interieur)"
    return ""


"""Il faut verifier que le message à coder n'as pour caractères que ceux
contenus dans l'alphabet exterieur pour crypter et interieur pour decrypter."""


def verif_correspondance_alberti(message, alphabet):
    for char in alphabet:
        # pour chaque lettre de l'alphabet, on supprime les identiques dans le message
        message = message.replace(char, '')
    # on remplace egalement les espaces
    message = message.replace(' ', '').replace('\n', '').replace('\t', '')
    if len(message) > 0:
        # s'il reste malgres tout des caractères
        # on les rescupère et les separe
        left = message.split()
        # on les ajoute les uns a la suite dans une chaine de caractères
        charsleft = "".join([left[i] + ',' for i in range(len(left))])
        # on renvoie un message d'erreur
        return "les caractères " + charsleft + " ne sont pas presents dans l'alphabet "
    # sinon on renvoie rien
    return ""


def decalage(array, decay):
    # si le decalage est nul, on revoie le tableau normal
    if decay == 0 or decay % len(array) == 0:
        return array
    temp = []
    # on recupere le decalage % taille du tableau pour connaitre le decalage effectif
    decay %= len(array)
    # i prends la valeur du decalage
    i = decay
    # on ajoute au tableau temporaire la premiere valeur du decalage
    temp.append(array[i])
    # on ajoute 1 à i
    i += 1
    while i != decay:
        # tant que i n'aura pas fait un tour complet (i = decay -> len(array) -> 0 -> decay)
        # on avance dans la boucle
        i %= len(array)
        # on ajoute la valeur i au tableau temporaire
        temp.append(array[i])
        # on augmente i de 1
        i += 1
    # on retourne le tableau temporaire
    return temp


def cr_alberti(message, keys):
    # on recupere les arguments
    decay = int(keys[0])
    incr = int(keys[1])
    periode = int(keys[2])
    exterieur = keys[3]
    interieur = keys[4]
    # on verifie la presence de caractères non desires.
    verif = verif_correspondance_alberti(message, exterieur)
    if verif != "":
        return verif + "exterieur"

    # on decale l'alphabet interieur du decalage initial
    interieur = decalage(interieur, decay)
    count_period = 0
    result = ''
    for char in message:
        # pour chaque caractère du message
        if char in exterieur:
            # si le caractère est inclus dans l'alphabet exterieur
            # on recupère sa position et on ajoute au resultat le caractère a la meme position de l'alphabet interieur
            result = ''.join([result, interieur[exterieur.index(char)]])
            # on incremente le compteur de periode
            count_period += 1
            # on le modulo avec la periode
            count_period %= periode
            # si la periode est atteinte, le compteur reviens a zero, alors:
            if count_period == 0:
                # on decale l'alphabet interieur de l'incrementation donnee
                interieur = decalage(interieur, incr)
        else:
            # sinon on l'ajoute comme tel dans le message code
            result = ''.join([result, char])
    return result


def de_alberti(code, keys):
    # on recupere les arguments
    decay = int(keys[0])
    incr = int(keys[1])
    periode = int(keys[2])
    exterieur = keys[3]
    interieur = keys[4]
    # on verifie la presence de caracteres non desires
    verif = verif_correspondance_alberti(code, interieur)
    if verif != "":
        return verif + "interieur"

    interieur = decalage(interieur, decay)
    count_period = 0
    result = ''
    for char in code:
        if char in interieur:
            # si le caractère est inclus dans l'alphabet interieur
            # on recupère sa position et on ajoute au resultat le caractère a la meme position de l'alphabet exterieur
            result = ''.join([result, exterieur[interieur.index(char)]])
            # on incremente le compteur de periode
            count_period += 1
            # on le modulo avec la periode
            count_period %= periode
            # si la periode est atteinte, le compteur reviens a zero, alors:
            if count_period == 0:
                # on decale l'alphabet interieur de l'incrementation donnee
                interieur = decalage(interieur, incr)
        else:
            # sinon on l'ajoute comme tel dans le message clair
            result = ''.join([result, char])
    # on renvoie le resultat
    return result

######RSA######


"""Fonction de verfication specifique pour la methode du RSA, pas de tests compleementaires"""


def verif_rsa(message, keys):
    # pas de cles a verifier
    return ""


from random import *

DEBUG = False

cleprivee = [71, 1073]

clepublique = [1079, 1073]


def texttoasciicode(text):
    code = ""
    for char in text:
        ordo = ord(char)
        # traduction des caracteres du message originel à leur code ASCII respectifs
        # char => code = 'C' => XXX (chiffres) si moins de 3 chiffres, ajout de 3-len(ord) 0
        code = "".join([code, (3 - len(str(ordo))) * '0', str(ordo)])
    if DEBUG:
        print("DEBUG: text en ascii: " + code)
    return code


def pgcd(a, b):  # Trouve le Plus Grand Commun Diviseur (PGCD) entre a et b
    while not b == 0:
        a, b = b, a % b
    return a


def rsa_isprime(n):  # Verifie si le nombre n est premier
    # Verification basique en premier temps
    if n < 7:
        if n in (2, 3, 5):
            return True
        else:
            return False
    # si n est pair et >2 (=2: cas traite ci-dessus), il ne peut pas être premier
    if n == 1:
        return False
    # autre cas, on va devoir le tester via la division euclidienne
    k = 3
    r = n ** 0.5
    while k <= r:
        if n % k == 0:
            return False
        k += 2
    return True


def rsa_genkeys(p=-1, q=-1):  # Fonction pour generer des cles à partir de p et q fournis optionellement.
    # ATTENTION, peut etre très long comme processus
    if p == -1:  # si p n'est pas renseigne on en prends un au hasard que l'on teste pour verifier qu'il soit premier
        p = randint(2500, 5000)  # s'il ne l'est pas, on recommence
        while not rsa_isprime(p):
            p = randint(2500, 5000)

    if q == -1:  # On fait de meme avec q
        q = randint(2500, 5000)
        while not rsa_isprime(q):
            q = randint(2500, 5000)

    n = p * q  # On calcule n et phi de n en fonction de p et q
    phiden = (p - 1) * (q - 1)
    plusgrand = p if p >= q else q  # on determine lequel de p ou de q est le plus grand
    e = 0  # on determine alors e, un nombre plus grand que p et q, inferieur à n et premier avec n
    for _e in range(plusgrand, n):
        if pgcd(n, _e) == 1:  # e et n sont premiers relatifs si leur pgcd est egal à 1
            e = _e
            break
    clepublique = [e, n]  # la cle publique sera donc composee de e et de n

    d = e ** (phiden - 1) % n  # d est alors calcule en fonction de e, phi de n et n
    cleprivee = [d, n]  # enfin la cle privee est comosee de d et n
    return [clepublique, cleprivee]


def getord(char):
    if char.isalpha():
        char = char.upper()
    ordo = ord(char)
    # on retourne le code ascii sous la forme de trois chiffres (si < 100 cela donne 0xx)
    return (len(str(ordo)) % 3) * "0" + str(ordo)


def cr_rsa(message):
    code = texttoasciicode(message)

    e = clepublique[0]
    n = clepublique[1]
    blocs = []
    count = 0
    line = ""
    for char in code:  # puis nous decoupons la suite de chiffres obtenue en blocs de 3
        count += 1
        line = "".join([line, char])
        if count == 3:
            blocs.append(line)
            line = ""
            count = 0
    if line != "":  # si le dernier block n'as pas ete ajoute (pas assez de chiffres pour faire un bloc de 3)
        line = "".join([line, (3 - len(line)) * "0"])  # alors on y ajoute les zeros supplementaires
        blocs.append(line)
    if DEBUG: print("DEBUG: blocs non cryptes => " + str(blocs))

    code = ""
    for bloc in blocs:  # enfin nous cryptons (calculons) les differents blocs obtenus via le modulo de n
        code = "".join([code, str(int(bloc) ** e % n), " "])  # et les ajoutons les uns à la suite separes
    # par des espaces pour former le message crypte
    return code[:-1:]  # Et virons le petit espace indesirable situe en bout de chaine. ^^


def de_rsa(code):
    d = cleprivee[0]
    n = cleprivee[1]
    blocs = code.split(' ')  # nous recuperons les differents blocs du message crypte
    blocentier = ""

    if DEBUG:
        print("DEBUG: blocs decryptes:", end=" ")

    for bloc in blocs:
        # Chaque bloc crypte va être decrypte en le multipliant par d
        # puis modulant par n et ajoute au reste du message decrypte
        blocdecrypte = int(int(bloc) ** d % n)
        # on y ajoute les zeros precedemment vires
        #  pour les nombres < à 100 par la conversion en int
        blocdecrypte = "".join([(3 - len(str(blocdecrypte))) * "0", str(blocdecrypte)])
        if DEBUG:
            print(" " + str(blocdecrypte), end="")
            # on ajoute au bloc dechiffre le bloc recalcule.
        blocentier = "".join([blocentier, str(blocdecrypte)])

    if DEBUG:
        print("\nDEBUG: message ascii decrypte: " + str(blocentier))

    message = ""
    for chiffre in range(0, len(blocentier), 3):
        # enfin nous retraduisons les codes ASCII sous leur forme de caractères
        # le nombre du caractère etant constitue de trois chiffres, on prends les
        # trois chiffres pour refaire le nombre et le traduisons en charactere
        number = str(blocentier[chiffre]) + str(blocentier[chiffre + 1]) + str(blocentier[chiffre + 2])
        if DEBUG:
            print("Nombre reconstitue: (" + number + ")" + str(int(number)))
        message = ''.join([message, chr(int(number))])  # ce nombre est reconverti en texte et ajoute au message clair
    return message


#####Systeme de Vernam#####

def verif_vernam(message, keys):
    # verification que le texte et la clé soient en caractères ascii:
    try:
        message.encode('ascii')
    except:
        return "Le message contient des accents ou n'est pas au format ascii."

    try:
        keys[0].encode('ascii')
    except:
        return "La clé contient des accents ou n'est pas au formet ascii"

    return ""


DEBUGVERNAM = True


def cr_vernam(message, keys):
    if DEBUGVERNAM:
        return "Ce cryptage est fonctionnel mais des difficultés techniques nous ont empéché de l'implémenter dans notre " \
               "interface actuelle. Pour la tester, ouvrez lib_cryptage.py et ajoutez en bas du fichier debugvernam(). " \
               "Puis appuyez sur F5."
    cle = keys[0]
    # on recupere la clé en forme binaire
    bytes_cle = bytearray(cle, 'ascii')
    # on recupere le message en forme binaire si il ne l'est pas déjà
    bytes_message = 0
    if isinstance(bytearray, type(message)):
        bytes_message = message
    else:
        bytes_message = bytearray(message, 'ascii')
    # le symbole  représente l'opération XOr, le OU exclusif en electronique
    # nous nous en servons car en binaire, le vernam se calcule tres simplemen
    # car message XOR cle = code
    # et code XOR cle = message
    # car ce systeme a pour fonctionnement de soustraire la cle au message
    # soit si on fait l'operation en binaire, on soustrait chaque bt de la cle a chaque bit du message ainsi
    # si bit_cle = 1 et bit_message = 1 -> 1-1 = 0
    # si bit_cle = 1 et bit_message = 0 -> 0-1 = 1 (en binaire il n'y a que des 0 et des 1)
    # si bit_cle = 0 et bit_message = 1 -> 1-0 = 1
    # si bit_cle = 0 et bit message = 0 -> 0-0 = 0
    # on as un 1 uniquement si l'un ou l'autre exclusivement contient 1 (ou exclusif ou xor)
    # on cree un tableau de bits de la taille du message
    bytes_code = bytearray('0' * len(bytes_message), 'ascii')
    for i in range(len(bytes_message)):
        bytes_code[i] = bytes_message[i] ^ bytes_cle[i]
    # le code binaire crypté est illisible en texte, on ne peut l'afficher que sous forme de tableau binaire pour le moment
    code = bytes_code
    # on renvoie le code
    return code


def de_vernam(code, keys):
    if DEBUGVERNAM:
        return "Ce cryptage est fonctionnel mais des difficultés techniques nous ont empéché de l'implémenter dans notre " \
               "interface actuelle. Pour la tester, ouvrez lib_cryptage.py et ajoutez en bas du fichier debugvernam(). " \
               "Puis appuyez sur F5."

    cle = keys[0]
    # on recupere la clé en forme binaire dans un tableau
    bytes_cle = bytearray(cle, 'ascii')
    # on recupere le code déjà sous forme de tableau s'il l'est déjà
    bytes_code = code
    # le symbole  représente l'opération XOR, le OU exclusif en electronique
    # nous nous en servons car en binaire, le vernam se calcule tres simplement
    # car message XOR cle = code
    # et code XOR cle = message
    # car ce systeme a pour fonctionnement de soustraire la cle au code
    # soit si on fait l'operation en binaire, on soustrait chaque bt de la cle a chaque bit du code ainsi
    # si bit_cle = 1 et bit_code = 1 -> 1-1 = 0
    # si bit_cle = 1 et bit_code = 0 -> 0-1 = 1 (en binaire il n'y a que des 0 et des 1)
    # si bit_cle = 0 et bit_code = 1 -> 1-0 = 1
    # si bit_cle = 0 et bit_code = 0 -> 0-0 = 0
    # on as un 1 uniquement si l'un ou l'autre exclusivement contient 1 (ou exclusif ou xor)
    # on cree un tableau de bits de la taille du message
    bytes_message = bytearray('0' * len(bytes_code), 'ascii')
    for i in range(len(bytes_code)):
        # pour chaque bit du message on le xor avec celui de la cle au meme index
        bytes_message[i] = bytes_code[i] ^ bytes_cle[i]
    # on retransforme le code binaire en texte
    message = bytes_message.decode('ascii')
    # on renvoie le texte
    return message


def debugvernam():
    global DEBUGVERNAM
    DEBUGVERNAM = False
    verif = " "
    message = ""
    while verif != "":
        print(verif)
        message = input("Votre message à crypter:\n\t")
        verif = verif_vernam(message, [len(message) * ' '])

    verif = " "
    cle = ""
    while verif != "":
        print(verif)
        cle = input("Votre clé:\n\t")
        while len(cle) < len(message):
            print("La clé doit être de taille supérieure ou égale au message à crypter!")
            cle = input("Votre clé:\n\t")
        verif = verif_vernam(message, [cle])

    code = cr_vernam(message, [cle])
    print("codé via vernam avec la bonne clé, cela donne en mode mémoire:\n\t" + str(
        code) + "\nEt en code caractères:\n\t" + str(list(code)))
    clair = de_vernam(code, [cle])
    print("decodé avec la bonne clé, cela donne\n\t" + str(clair))
    # prenons 43% de bon de la clé: 43*len(cle)/100 =43 % de la longueur de la clé
    import math
    # on prends le debut de la clé jusqu'à 43%
    debut = cle[:math.floor(43 * len(cle) / 100):]
    # pour remplir la nouvelle clé au meme nombre de caractères, on recupère la difference de longueur. manquant =  cle - debut
    reste = len(cle) - len(debut)
    # on remplis alors le reste avec un caractere pour fausser la fin de la clé
    fin = reste * ','
    # on rassemble notre nouvelle clé
    cle2 = debut + fin
    clair2 = de_vernam(code, [cle2])
    print("décodé avec une clé partiellement bonne, cela donne\n\t" + str(clair2))
    DEBUGVERNAM = True
